<?php


namespace Anesh\Deployment\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Filesystem\Filesystem;

class Developer extends Command
{
	protected $_scopeConfig;
	protected $_dir;
	protected $_mageRoot;
	protected $_logger;
	
	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Psr\Log\LoggerInterface $logger
	){
		$this->_scopeConfig = $scopeConfig;
		$this->_dir = $dir;
		$this->_mageRoot = $this->_dir->getRoot();
        $this->_logger = $logger;
        parent::__construct();
    }
	
	/**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
		$commandsRaw = $this->_scopeConfig->getValue('deployment/developer/commands', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$commandsArray = explode(PHP_EOL, $commandsRaw);
		foreach($commandsArray as $commandStr){
			$commandStr = str_replace("\r", '', $commandStr);
			$commandStr = str_replace("\n", '', $commandStr);
			$commandStr = str_replace("MAGE_ROOT", $this->_mageRoot, $commandStr);
			$output->writeln($commandStr);
			$outputArr = array();
			$returnVar = array();
			$string = exec($commandStr, $outputArr, $returnVar);
			$outputString = implode(PHP_EOL, $outputArr);
			$output->writeln($outputString);
		}
    }
	
    /**
     * {@inheritdoc}
     */
    /*protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
		$commandsArray = array(
			array(
				'command' => 'deploy:mode:set',
				'arguments' => array(
					'command' => 'deploy:mode:set',
					'mode' => 'developer',
				),
				'comment' => '-------------------------- Running deploy:mode:set developer --------------------------'
			),
			array(
				'command' => 'setup:upgrade',
				'arguments' => array(
					'command' => 'setup:upgrade'
				),
				'comment' => '-------------------------- Running setup:upgrade --------------------------'
			),
			array(
				'command' => 'indexer:reindex',
				'arguments' => array(
					'command' => 'indexer:reindex'
				),
				'comment' => '-------------------------- Running indexer:reindex --------------------------'
			),
			array(
				'command' => 'cache:clean',
				'arguments' => array(
					'command' => 'cache:clean'
				),
				'comment' => '-------------------------- cache:clean --------------------------'
			),
			array(
				'command' => 'cache:flush',
				'arguments' => array(
					'command' => 'cache:flush'
				),
				'comment' => '-------------------------- Running cache:flush --------------------------'
			)
		);
		$fs = new Filesystem();
        try {
			if($fs->exists('var/cache')){
                $fs->remove(array('var/cache'));
				$output->writeln('rm -rf var/cache');
            }
			if($fs->exists('var/page_cache')){
                $fs->remove(array('var/page_cache'));
				$output->writeln('rm -rf var/page_cache');
            }
			if($fs->exists('var/view_preprocessed')){
                $fs->remove(array('var/view_preprocessed'));
				$output->writeln('rm -rf var/view_preprocessed');
            }
			if($fs->exists('var/composer_home/cache')){
                $fs->remove(array('var/composer_home/cache'));
				$output->writeln('rm -rf var/composer_home/cache');
            }
			if($fs->exists('var/generation')){
                $fs->remove(array('var/generation'));
				$output->writeln('rm -rf var/generation');
            }
			if($fs->exists('generated/code')){
                $fs->remove(array('generated/code'));
				$output->writeln('rm -rf generated/code');
            }
			if($fs->exists('generated/metadata')){
                $fs->remove(array('generated/metadata'));
				$output->writeln('rm -rf generated/metadata');
            }
			if($fs->exists('var/di')){
                $fs->remove(array('var/di'));
				$output->writeln('rm -rf var/di');
            }
			if($fs->exists('pub/static/_requirejs')){
                $fs->remove(array('pub/static/_requirejs'));
				$output->writeln('rm -rf pub/static/_requirejs');
            }
			if($fs->exists('pub/static/adminhtml')){
                $fs->remove(array('pub/static/adminhtml'));
				$output->writeln('rm -rf pub/static/adminhtml');
            }
			if($fs->exists('pub/static/frontend')){
                $fs->remove(array('pub/static/frontend'));
				$output->writeln('rm -rf pub/static/frontend');
            }
			if($fs->exists('pub/static/deployed_version.txt')){
                $fs->remove(array('pub/static/deployed_version.txt'));
				$output->writeln('rm -rf pub/static/deployed_version.txt');
            }
        } catch (IOExceptionInterface $e) {
			$output->writeln($e->getPath());
        }
		foreach($commandsArray as $commandItem){
			$output->writeln($commandItem['comment']);
			$commandStr = $commandItem['command'];
			$command = $this->getApplication()->find($commandStr);
			$arguments = $commandItem['arguments'];
			$greetInput = new ArrayInput($arguments);
			$returnCode = $command->run($greetInput, $output);
		}
    }*/

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("deployment:developer");
        $this->setDescription("Deployment process to set site to Developer mode.");
        $this->setDefinition([]);
        parent::configure();
    }
}
