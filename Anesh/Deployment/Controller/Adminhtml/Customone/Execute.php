<?php


namespace Anesh\Deployment\Controller\Adminhtml\Customone;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Execute extends \Magento\Backend\App\Action
{

    protected $resultJsonFactory;
    protected $_scopeConfig;
    protected $_dir;
    protected $_mageRoot;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        JsonFactory $resultJsonFactory
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_dir = $dir;
        $this->_mageRoot = $this->_dir->getRoot();
        parent::__construct($context);
    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $report = '';
        try {
            $commandsRaw = $this->_scopeConfig->getValue('deployment/customone/commands', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $commandsArray = explode(PHP_EOL, $commandsRaw);
            $report = '';
            foreach($commandsArray as $commandStr){
                $commandStr = str_replace("\r", '', $commandStr);
                $commandStr = str_replace("\n", '', $commandStr);
                $commandStr = str_replace("MAGE_ROOT", $this->_mageRoot, $commandStr);
                //$output->writeln($commandStr);
                $report .= '<b>'.$commandStr.'</b>'.PHP_EOL;

                $outputArr = array();
                $returnVar = array();
                $string = exec($commandStr, $outputArr, $returnVar);
                $outputString = implode(PHP_EOL, $outputArr);
                //$output->writeln($outputString);
                $report .= $outputString.PHP_EOL;
                $report = str_replace(PHP_EOL, '<br/>', $report);
            }
        } catch (\Exception $e) {
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $report = $e->getMessage();
        }

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        return $result->setData(['success' => true, 'report' => $report]);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Anesh_Deployment::config');
    }
}
